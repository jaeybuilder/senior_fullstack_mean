﻿export class Club {
  name: string;
  text: string;
  image: string;
  userId: string[];
  _id: string;
  created: string;
  __v: any;
}

export class ClubsByPage {
  clubs: Club[];
  totalPages: number;
  currentPage: number;
}
