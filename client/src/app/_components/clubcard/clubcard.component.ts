import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/_models';
import { Club } from '@app/_models/clubs';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-clubcard',
  templateUrl: './clubcard.component.html',
  styleUrls: ['./clubcard.component.scss'],
})
export class ClubcardComponent implements OnInit {
  @Input() user: User;
  @Input() club: Club;
  @Output() putSubscribeUser;
  @Output() putUnsubscribeUser;

  constructor(public dialog: MatDialog) {
    this.putSubscribeUser = new EventEmitter();
    this.putUnsubscribeUser = new EventEmitter();
  }

  ngOnInit(): void {}

  openDialog(subscribe: boolean, club: Club): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '450px',
      data: { subscribe: subscribe, club: club },
    });

    dialogRef.afterClosed().subscribe((result) => {
      result &&
        (subscribe
          ? this.putSubscribeUser.emit(club._id)
          : this.putUnsubscribeUser.emit(club._id));
    });
  }
}
