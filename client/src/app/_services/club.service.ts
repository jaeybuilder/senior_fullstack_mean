﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class ClubService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(private router: Router, private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('user'))
    );
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  getAll() {
    return this.http.get<any>(`${environment.apiUrl}/clubs`);
  }

  getByPage(page: number) {
    return this.http.get<any>(
      `${environment.apiUrl}/clubs/bypage?page=${String(page)}`
    );
  }

  putSubscribeUser(body: { user: string; club: string }) {
    return this.http.put<any>(
      `${environment.apiUrl}/clubs/subscribeUser`,
      body
    );
  }

  putUnsubscribeUser(body: { club: string }) {
    return this.http.put<any>(
      `${environment.apiUrl}/clubs/unsubscribeUser`,
      body
    );
  }
}
