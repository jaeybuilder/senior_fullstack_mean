﻿import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { User } from '@app/_models';
import { Club, ClubsByPage } from '@app/_models/clubs';
import { AccountService, AlertService } from '@app/_services';
import { ClubService } from '@app/_services/club.service';
import { first } from 'rxjs/operators';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
  user: User;
  clubs: Club[];
  currentPage: number;
  totalPages: number;
  listType: 'all' | 'paginated';

  constructor(
    private accountService: AccountService,
    private clubService: ClubService,
    private snackBar: MatSnackBar
  ) {
    this.user = this.accountService.userValue;
    this.clubs = [];
    this.currentPage = 1;
    this.listType = 'all';
  }

  ngOnInit() {
    this.getClubs();
  }

  getClubs() {
    this.clubService
      .getAll()
      .pipe(first())
      .subscribe({
        next: (res: Club[]) => {
          this.clubs = res;
        },
        error: (error) => {
          this.snackBar.open(error, 'OK', {
            panelClass: 'error-snackbar',
          });
        },
      });
  }

  getClubsBypage() {
    this.clubService
      .getByPage(this.currentPage)
      .pipe(first())
      .subscribe({
        next: (res: ClubsByPage) => {
          const { clubs, currentPage, totalPages } = res;
          this.clubs = clubs;
          this.currentPage = currentPage;
          this.totalPages = totalPages;
        },
        error: (error) => {
          this.snackBar.open(error, 'OK', {
            panelClass: 'error-snackbar',
          });
        },
      });
  }

  putSubscribeUser(club: string) {
    this.clubService
      .putSubscribeUser({ user: this.user._id, club: club })
      .pipe(first())
      .subscribe({
        next: (res) => {
          const temp: Club = this.clubs.find((c) => c._id === club);
          !temp.userId.includes(this.user._id) &&
            temp.userId.push(this.user._id);
          this.clubs = this.clubs.map((c) =>
            c._id === res._id ? { ...c, userId: temp.userId } : c
          );
        },
        error: (error) => {
          this.snackBar.open(error, 'OK', {
            panelClass: 'error-snackbar',
          });
        },
      });
  }

  putUnsubscribeUser(club: string) {
    this.clubService
      .putUnsubscribeUser({ club: club })
      .pipe(first())
      .subscribe({
        next: (res) => {
          this.clubs = this.clubs.map((c) =>
            c._id === res._id
              ? { ...c, userId: c.userId.filter((id) => id !== this.user._id) }
              : c
          );
        },
        error: (error) => {
          this.snackBar.open(error, 'OK', {
            panelClass: 'error-snackbar',
          });
        },
      });
  }

  changeList(e: any) {
    this.listType = e.value;
    this.listType === 'all' ? this.getClubs() : this.getClubsBypage();
  }

  changePage(action: 'plus' | 'minus') {
    action === 'plus' ? this.currentPage++ : this.currentPage--;
    this.getClubsBypage();
  }
}
