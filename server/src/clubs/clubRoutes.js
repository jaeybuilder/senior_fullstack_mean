"use strict";

var clubController = require("./clubController");

module.exports = function (app) {
  app.route("/clubs").get(clubController.list);

  app.route("/clubs/bypage").get(clubController.listByPage);

  app.route("/clubs/subscribeUser").put(clubController.subscribeUser);

  app.route("/clubs/unsubscribeUser").put(clubController.unsubscribeUser);
};
