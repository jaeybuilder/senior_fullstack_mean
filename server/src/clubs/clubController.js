"use strict";

var mongoose = require("mongoose");
var Club = mongoose.model("Club");

module.exports.list = (req, res) => {
  Club.find()
    .then((c) => res.jsonp(c))
    .catch((error) => res.status(500).send({ message: error }));
};

module.exports.listByPage = async (req, res) => {
  // destructure page and limit and set default values
  console.log(req.query);
  const { page = 1, limit = 10 } = req.query;

  try {
    // execute query with page and limit values
    const clubs = await Club.find()
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .exec();

    // get total documents in the Posts collection
    const count = await Club.count();

    // return response with posts, total pages, and current page
    res.json({
      clubs,
      totalPages: Math.ceil(count / limit),
      currentPage: page,
    });
  } catch (err) {
    console.error(err.message);
  }
};

module.exports.subscribeUser = async (req, res) => {
  const { user, club } = req.body;
  const filter = { _id: club };

  try {
    const club = await Club.findOne(filter);
    !club.userId.includes(user) && club.userId.push(user);
    Club.findOneAndUpdate(
      filter,
      { userId: club.userId },
      {
        new: true,
      }
    ).then((c) => res.jsonp(c));
  } catch (error) {
    res.status(500).send({ message: error });
  }
};

module.exports.unsubscribeUser = async (req, res) => {
  const { user, club } = req.body;
  const filter = { _id: club };

  try {
    const club = await Club.findOne(filter);
    Club.findOneAndUpdate(
      filter,
      { userId: club.userId.filter((id) => id !== user) },
      {
        new: true,
      }
    ).then((c) => res.jsonp(c));
  } catch (error) {
    res.status(500).send({ message: error });
  }
};
