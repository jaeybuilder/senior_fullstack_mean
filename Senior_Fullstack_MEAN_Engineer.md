ODILO Senior Fullstack MEAN Engineer Test
=========================================

# PRUEBA DE CÓDIGO #


En esta prueba se valorará la resolución de los enunciados y sobre todo las buenas prácticas de código y el diseño de interfaz, responsabilidades..

Se pueden añadir las dependencias necesarias, así como el uso de componentes externos.

También puedes refactorizar el código ofrecido o incluso modificar el modelo, si lo ves conveniente.

Queda de tu mano incluir o mejorar la gestión de errores, seguridad, añadir test o cualquier cosa que veas conveniente.. valoraremos ese esfuerzo adicional.

El plazo es abierto, cuanto antes lo tengas antes lo revisamos. Como regla general es algo que podría implementarse de forma básica en 2-4 horas, pero damos normalmente una semana por encajarlo en los huecos libres y tal.

## Instrucciones de entrega ## 

Cuando termines:

- Envía un ZIP con el proyecto: 
{your_first_name}-{your_last_name}-fullstack.zip

- O bien publicas en un repo personal y nos das acceso 

En cualquier caso, a las personas que te envíemos el mail de la práctica nos incluyes en el mail/invitación.

Mucha suerte!!


## Club de lectura ##


Se quiere implementar una aplicación de un club de lectura, donde usuarios podrán entrar a esta plataforma y navegar por los diferentes clubs de lectura, donde podrás encontrar cientos/miles de clubs potencialmente. 

Partimos entonces, de usuarios y clubs como modelo de datos. 

Este proyecto parte con un cliente en angular y la parte servidor en node 12 y mongodb, ambos funcionales.

El arquetipo funcional está en la carpeta resources/Senior_Fullstack_MEAN. Descargalo en un repo personal y trabaja sobre esa versión.

Requerimiento no funcional: Se asume que el proyecto potencialmente podría tener altas volumetrías de usuarios o clubs.


Implementar:

* Listar todos los clubs de lectura en la home, diferenciando entre los que sigue el usuario y los que no sigue

* Permitir a un usuario seguir un club de lectura concreto y reflejarlo en la interfaz